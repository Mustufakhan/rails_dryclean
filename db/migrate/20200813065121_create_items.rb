class CreateItems < ActiveRecord::Migration[6.0]
  def change
    create_table :items do |t|
      t.text :item_type
      t.integer :category_id

      t.timestamps
    end
  end
end
