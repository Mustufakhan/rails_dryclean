class RemoveUpperFromCategories < ActiveRecord::Migration[6.0]
  def change
    remove_column :categories, :upper, :string
  end
end
