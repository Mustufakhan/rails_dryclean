class RemoveLowerFromCategories < ActiveRecord::Migration[6.0]
  def change
    remove_column :categories, :lower, :string
  end
end
