class AddItemIdToBookings < ActiveRecord::Migration[6.0]
  def change
    add_column :bookings, :item_id, :string
  end
end
