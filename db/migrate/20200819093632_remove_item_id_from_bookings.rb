class RemoveItemIdFromBookings < ActiveRecord::Migration[6.0]
  def change
    remove_column :bookings, :item_id, :integer
  end
end
