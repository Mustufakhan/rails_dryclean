Rails.application.routes.draw do
  root 'user#index'

  resources :user do
  	collection do
      get :login_user
    end		
  end

  resources :bookings

end
