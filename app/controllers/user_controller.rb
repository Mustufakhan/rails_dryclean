class UserController < ApplicationController        

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      binding.pry
      redirect_to user_index_path
    else
      render 'new'
    end
  end

  def login_user
    if params[:email].present? && params[:password].present? 
      user = User.where(email: params[:email], password: params[:password])
      unless user.blank?
        binding.pry
        session['user_id'] = user.ids
        redirect_to new_booking_path, notice: "You have successfully logged in"
      end
    end
  end  

  private
  
  def user_params
    params.require(:user).permit(:username, :email, :password, :user_type)
  end 
end

