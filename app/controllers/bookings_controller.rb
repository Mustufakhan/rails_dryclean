class BookingsController < ApplicationController
   
  def index
   @item = Item.all
  end

  def new
    @booking = Booking.new
  end

  def create
    binding.pry
    @booking = Booking.new(booking_params)
    if @booking.save

      redirect_to bookings_path, notice: "Booking created"
    else
      render 'new' ,alert: "Something went wrong"
    end
  end	

  private
    def booking_params
      params.require(:booking).permit(:status,:item_id => [])
    end 
end
